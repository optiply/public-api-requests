# Public API requests

This repository holds several different example requests to access the Optiply public API.

## Optiply Public API

The Optiply public API is available to subscribed users. It loosely follows the jsonapi spec. The API can be used to read, create and update resources. Should you have any questions about usage or suggestions to present, please contact support@optiply.nl .

Please send us an email when you would like to request more information, documentation or help with building your integration.

### Insomnia
[Insomnia](https://insomnia.rest/) is an open source API client. Learn more [here](https://support.insomnia.rest/category/149-getting-started).
